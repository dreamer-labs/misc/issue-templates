#### TITLE
<hr/>
Verb Network Noun
<hr/>

#### DESCRIPTION
<hr/>

(not applicable)

<hr/>


#### USAGE
<hr/>

Epics are used to organize sets of tasks and user stories into projects that can be tracked by project sponsors.  Not all epics will be sponsored (as some will be internal) but for those projects that are the sponsor should be added to the assignee field.