#### TITLE
<hr/>
Persona wants outcome
<hr/>

#### DESCRIPTION
<hr/>

**As** persona, **I want** outcome, **so that** reason.

**Given** a_certain_application_state \
**When** a_user_takes_an_action \
**Then** another_application_state

**Notes**

story_notes
<hr/>


#### USAGE
<hr/>

User stories describe an outcome desired by a given user group (represented by a persona).  In the IPA workflow they represent the user value delivered once an Epic is completed.  Because of this, a story should be assigned an epic upon creation.